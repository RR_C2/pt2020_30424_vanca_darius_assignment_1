package Controller;

import Model.*;
import View.View;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class Controller {

    private View view;
    private Model model;

    public Controller(View view, Model model){

        this.view = view;
        this.model = model;

        this.view.addGoButtonListener(new goButtonListener());
        this.view.addCBListener(new cbListener());
    }

    class cbListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            view.setResult("");

            if(view.getOption().equals("Integration") || view.getOption().equals("Derivation"))
                view.setTextField2Visible(false);
            else
                view.setTextField2Visible(true);
        }
    }

    class goButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            try {

                model.setFirstPoly(view.getFirstPoly());

            }catch (IOException exc){

                JOptionPane.showMessageDialog(view, exc.getMessage());
                view.setTextField1("");
                view.setResult("");
                return;
            }

            try{

                model.setSecondPoly(view.getSecondPoly());

            }catch (IOException exc){

                JOptionPane.showMessageDialog(view, exc.getMessage());
                view.setTextField2("");
                view.setResult("");
                return;
            }

            switch (view.getOption()) {

                case "Addition" :
                    view.setResult(model.calculateSum().toString());
                    break;

                case "Subtraction" :
                    view.setResult(model.calculateDifference().toString());
                    break;

                case "Multiplication" :
                    view.setResult(model.multiply().toString());
                    break;

                case "Division" :

                    Polynomial[] result = new Polynomial[2];
                    try {
                        result = model.divideBy();
                        view.setResult("Q:" + result[0].toString() + " r:" + result[1].toString());
                    }catch(Exception exc){

                        JOptionPane.showMessageDialog(view, exc.getMessage());
                    }
                    break;

                case "Integration" :
                    view.setResult(model.integrate().toString());
                    break;

                case "Derivation" :
                    view.setResult(model.derivate().toString());
                    break;

                default :
                    break;

            }
        }
    }

    public static void main(String[] args) {

        Controller control = new Controller(new View(), new Model());
    }
}
