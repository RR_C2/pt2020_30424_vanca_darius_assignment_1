package Model;

import javax.swing.plaf.synth.SynthOptionPaneUI;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validation {

/**
 * Static method used to validate the input of the UI
 * @param in string which could be used to create a polynomial
 * @return the same string
 * @throws IOException in case the input string doesn't match the required pattern
 * **/
    public static String validate(String in) throws IOException {

        if(in.contains("-")) {

            Pattern p = Pattern.compile("-");
            Matcher m = p.matcher(in);
            in = m.replaceAll("+-");
        }

        String[] mono = in.split("\\+");

        Pattern p2 = Pattern.compile("^([-])?([0-9])*+(X\\^[0-9]+?)?");
        Pattern p3 = Pattern.compile("^[-]?+[0-9]*+");
        Pattern p4 = Pattern.compile("^[-]?+[0-9]*+X");

        for(String c : mono){

            Matcher m = p2.matcher(c);

            if(!m.matches()) {

                m = p4.matcher(c);

                if(!m.matches()){

                    m = p3.matcher(c);

                    if(!m.matches()) {
                        throw new IOException("Invalid input for:" + in);
                    }
                }
            }

        }

        return in;
    }
/**
 * Static method used to validate a Number
 * @param in a number which needs validation
 * @return the correct form of the number ( int or double )
 * **/
    public static Number getGoodNumber(Number in){

        DecimalFormat df = new DecimalFormat("0.00");

        if(in.doubleValue() == Math.floor(in.doubleValue()))
            return in.intValue();
        else
            return Double.parseDouble(df.format(in.doubleValue()));
    }
}
