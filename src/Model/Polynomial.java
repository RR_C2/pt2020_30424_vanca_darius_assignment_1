package Model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Polynomial {

    private ArrayList<Monomial> monomials;
    public ArrayList<Monomial> getMonomials() {
        return monomials;
    }
/**
 * Constructor used to create the polynomial from a String
* @param in is a String which will be used to create the polynomial
* */
    public Polynomial(String in){

        monomials = new ArrayList<Monomial>();

        if(in.equals(""))
            monomials.add(new Monomial(0, 0));
        else {

            if (in.contains("-")) {//check for negative coefficients

                Pattern p = Pattern.compile("-");
                Matcher m = p.matcher(in);
                in = m.replaceAll("+-");//replace "-" with "+-" to make splitting and adding to the list of monomials easier
            }

            String[] mono = in.split("\\+");
            String[] aux;

            for (String c : mono) {//search in the resulted strings the coefficients based on the case

                if (c.contains("^")) {//String is of form "aX^b"

                    aux = c.split("X\\^");//get the coefficient and the power, aux[0] contains the coefficient, aux[1] the power

                    if (c.matches("X\\^(.*)"))//no coefficient ( meaning "1" actually )
                        monomials.add(new Monomial(1, Integer.parseInt(aux[1])));
                    else if (c.matches("-X\\^(.*)"))//negative case
                        monomials.add(new Monomial(-1, Integer.parseInt(aux[1])));
                    else if (!aux[0].equals("0") && !aux[0].equals(""))//normal case
                        monomials.add(new Monomial(Integer.parseInt(aux[0]), Integer.parseInt(aux[1])));

                } else if (c.contains("X")) {//String is of form "aX", power is "1" in this case

                    aux = c.split("X");//almost the same steps as above, just take into consideration that the power is "1" in this case

                    if (c.compareTo("X") == 0)
                        monomials.add(new Monomial(1, 1));
                    else if (c.compareTo("-X") == 0)
                        monomials.add(new Monomial(-1, 1));
                    else if (!aux[0].equals("0") && !aux[0].equals(""))
                        monomials.add(new Monomial(Integer.parseInt(aux[0]), 1));


                } else if (!c.equals("0") && !c.equals("")) {//if not 0 or ""(empty) then String is of form "a" ( just the coefficient, meaning power is 0 )

                    monomials.add(new Monomial(Integer.parseInt(c), 0));
                }

            }

            monomials.sort(Collections.reverseOrder());
            this.selfCorrect();
        }
    }
/**
 * Functions which is used to for getting the correct form of a polynomial.
 * A polynomial is considered to be in a correct form if it is reverse sorted order based on the power and if there are no 2 monomials which have the same degree ( power ).
 * **/
    private void selfCorrect(){

        double sum = 0;
        boolean matchFound = false;//flag

        for(int i = 0; i < monomials.size() - 1; i++){//knowing that the polynomial is already sorted based on the power

            sum = 0;//initialize the sum
            while (monomials.get(i).compareTo(monomials.get(i + 1)) == 0) {//while 2 adjacent monomials have the same degree add them

                matchFound = true;//set the flag

                sum += monomials.get(i).getCoefficient().doubleValue();
                monomials.remove(i);//remove the old monomial

                if (i + 1 >= monomials.size())//stop if out of range
                    break;
            }

            if (matchFound) {//because of the remove() method we lost 1 monomial in the loop so we need to add him here

                monomials.add(new Monomial(Validation.getGoodNumber(sum + monomials.get(i).getCoefficient().doubleValue()), monomials.get(i).getPower()));
                monomials.remove(i);
                matchFound = false;
                i--;//be careful around the "for()", after removing element "i" our current element should still be "i" because we are dealing with a list not a normal array
            }
        }

        monomials.sort(Collections.reverseOrder());

    }
/**
 * Parameterless constructor
 * **/
    public Polynomial(){
        monomials = new ArrayList<Monomial>();
    }
/**Constructor which creates a new polynomial out of another one
 * @param p a polynomial used to create a new one
 * **/
    public Polynomial(Polynomial p){
        monomials = new ArrayList<Monomial>(p.getMonomials());
    }

    public int degree(){ return monomials.get(0).getPower(); }/**@return method used to return the degree of a polynomial**/

    public Monomial lead(){ return monomials.get(0); }/**@return method used to return the lead**/

    public boolean isZero(){/**@return true if polynomial is zero, and false otherwise**/

        for(Monomial m : monomials)
            if(m.getCoefficient().doubleValue() != 0.0)
                return false;

        return true;
    }
/**Method used to add 2 polynomials
 * @param a is another polynomial used for the addition
 * @return a new polynomial resulted as the addition of the 2 polynomials**/
    public Polynomial plus(Polynomial a){

        Polynomial result = new Polynomial();
        ArrayList<Integer> powerList = new ArrayList<Integer>();//used for the monomials who don't have matches in the other polynomial
        boolean matchFound = false;

        for(Monomial m : a.getMonomials())
            powerList.add(m.getPower());//at the beginning the power list is full with all the powers from the 2nd polynomial ( used to uniquely identify the monomial )

        for(Monomial m1 : monomials){
            for (Monomial m2 : a.getMonomials()) {

                if(m1.getCoefficient().doubleValue() + m2.getCoefficient().doubleValue() == 0 && m1.compareTo(m2) == 0) {//if by adding them you'd get a zero as a coefficient ignore it, and remove power from powerlist

                    matchFound = true;//set the flag
                    powerList.remove((Integer) m2.getPower());//not suitable for result

                }else if(m1.compareTo(m2) == 0){//if they share the same degree and the addition is different from zero

                    matchFound = true;
                    powerList.remove((Integer)m2.getPower());//we don't need to add it to the result
                    result.getMonomials().add(new Monomial(Validation.getGoodNumber(m1.getCoefficient().doubleValue() + m2.getCoefficient().doubleValue()), m1.getPower()));//add them
                }
            }

            if(!matchFound)//if no match found after comparing the current monomial from the first polynomial to all the monomials from the 2nd polynomial just add it to the result
                result.getMonomials().add(new Monomial(Validation.getGoodNumber(m1.getCoefficient().doubleValue()), m1.getPower())); matchFound = false;
        }

        for(Monomial m : a.getMonomials())//the powers which remain are the ones who don't have a match so we need to add the monomials
            if(powerList.contains(m.getPower()))
                result.getMonomials().add(new Monomial(Validation.getGoodNumber(m.getCoefficient().doubleValue()), m.getPower()));

        result.getMonomials().sort(Collections.reverseOrder());

        return result;
    }
    /**Method used to subtract 2 polynomials
     * @param a is another polynomial used for the subtraction
     * @return a new polynomial resulted as the subtraction of the 2 polynomials**/
    public Polynomial minus(Polynomial a){//the approach is identical to the one from the "plus()" method

        Polynomial result = new Polynomial();
        ArrayList<Integer> powerList = new ArrayList<Integer>();
        boolean matchFound = false;

        for(Monomial m : a.getMonomials())
            powerList.add(m.getPower());

        for(Monomial m1 : monomials){
            for (Monomial m2 : a.getMonomials()) {

                if(m1.getCoefficient().doubleValue() - m2.getCoefficient().doubleValue() == 0 && m1.compareTo(m2) == 0){

                    matchFound = true;
                    powerList.remove((Integer)m2.getPower());

                }else if(m1.compareTo(m2) == 0){

                    matchFound = true;
                    result.getMonomials().add(new Monomial(Validation.getGoodNumber(m1.getCoefficient().doubleValue() - m2.getCoefficient().doubleValue()), m1.getPower()));
                    powerList.remove((Integer)m2.getPower());
                }
            }

            if(!matchFound)
                result.getMonomials().add(new Monomial(Validation.getGoodNumber(m1.getCoefficient().doubleValue()), m1.getPower())); matchFound = false;
        }

        for(Monomial m : a.getMonomials())
            if(powerList.contains((Integer)m.getPower()))
                result.getMonomials().add(new Monomial(Validation.getGoodNumber(-m.getCoefficient().doubleValue()), m.getPower()));

        result.getMonomials().sort(Collections.reverseOrder());

        return result;
    }
/**Method used to multiply 2 polynomials
 * @param a another polynomial
 * @return a new polynomial resulted from the multiplication**/
    public Polynomial multiplyBy(Polynomial a){

        Polynomial result = new Polynomial();

        for(Monomial m1 : monomials)
            for(Monomial m2 : a.getMonomials())//simply multiply all the the monomials between them
                if(m1.getCoefficient().doubleValue() * m2.getCoefficient().doubleValue() != 0.0 )
                    result.getMonomials().add(new Monomial(Validation.getGoodNumber(m1.getCoefficient().doubleValue() * m2.getCoefficient().doubleValue()), m1.getPower() + m2.getPower()));

        result.monomials.sort(Collections.reverseOrder());
        result.selfCorrect();//get the correct form of the polynomial

        return result;
    }
/**@return a String which is the representation of the polynomial**/
    @Override
    public String toString() {

        String fString = "";//String buffer
        boolean start = true;//flag for the beginning of the string

        monomials.sort(Collections.reverseOrder());//make sure it is reversed

        for(Monomial current: monomials){

            if(start){//if at the beginning of the string deal with the corresponding cases

                if(current.getPower() == 0)//form is "a", just the coefficient
                    fString = current.getCoefficient() + "";
                else if(current.getPower() == 1) {//form is "aX"

                    if(current.getCoefficient().doubleValue() == 1.0)
                        fString = "X";
                    else if(current.getCoefficient().doubleValue() == -1.0)
                        fString = "-X";
                    else
                        fString = current.getCoefficient() + "X";
                }
                else {//form is "aX^b"

                    if (current.getCoefficient().doubleValue() == 1.0)
                        fString = "X^" + current.getPower();
                    else if(current.getCoefficient().doubleValue() == -1.0)
                        fString = "-X^" + current.getPower();
                    else
                        fString = current.getCoefficient() + "X^" + current.getPower();
                }

                start = false;
                continue;//skip the rest of the code for this iteration
            }
            //if not the first iteration do the same thing as the code from above but don't forget to link the monomials with "+" ( assuming the coefficient is positive )
            if(current.getPower() == 0) {

                if(current.getCoefficient().doubleValue() < 0)
                    fString = fString + current.getCoefficient();
                else
                    fString = fString + "+" + current.getCoefficient();

            }else if(current.getPower() == 1) {

                if(current.getCoefficient().doubleValue() == 1.0)
                    fString = fString + "+" + "X";
                else if(current.getCoefficient().doubleValue() == -1.0)
                    fString = fString + "-X";
                else if (current.getCoefficient().doubleValue() < 0)
                    fString = fString + current.getCoefficient() + "X";
                else
                    fString = fString + "+" + current.getCoefficient() + "X";
            }
            else {

                if(current.getCoefficient().doubleValue() == 1.0)
                    fString = fString + "+" + "X^" + current.getPower();
                else if(current.getCoefficient().doubleValue() == -1.0)
                    fString = fString + "-X^" + current.getPower();
                else if(current.getCoefficient().doubleValue() < 0)
                    fString = fString + current.getCoefficient() + "X^" + current.getPower();
                else
                    fString = fString + "+" + current.getCoefficient() + "X^" + current.getPower();
            }
        }
        return (fString.equals("") ? "0" : fString);
    }
}
