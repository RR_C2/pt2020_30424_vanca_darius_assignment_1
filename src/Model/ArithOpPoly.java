package Model;

import org.omg.PortableInterceptor.INACTIVE;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ArithOpPoly extends Polynomial {

/**@param in String used to create the polynomial**/
    public ArithOpPoly(String in){
        super(in);//call the super constructor
    }

    public ArithOpPoly(){
        super();//call the parameterless super constructor
    }

    public ArithOpPoly(Polynomial p){
        super(p);
    }
/**Method used to perform the derivation operation
 * @return No**/
    public Polynomial derivate(){

        Monomial toDelete = null;//in case we have monomials with power = 0

        for(Monomial m : super.getMonomials()){

            if(m.getPower() != 0) {

                m.setCoefficient(m.getCoefficient().intValue() * m.getPower());
                m.setPower(m.getPower() - 1);
            }
            else
                toDelete = m;
        }

        if(toDelete != null)
            super.getMonomials().remove(toDelete);

        return this;
    }
/**Method used to perform the integration operation
 * @return No**/
    public Polynomial integrate(){

        for(Monomial m : super.getMonomials()){

            m.setCoefficient(Validation.getGoodNumber(m.getCoefficient().doubleValue() / (m.getPower() + 1)));
            m.setPower(m.getPower() + 1);

        }

        return this;
    }
/**Method used to perform the division operation
 *
 * @param p another polynomial used int he division operation
 * @return 2 polynomials, the quotient and the remainder**/
    public Polynomial[] divideBy(Polynomial p) throws Exception{

        try{

            if(p.isZero())
                throw new Exception("division by zero not allowed");
        }catch (Exception e){
            throw e;
        }

        Polynomial q = new ArithOpPoly("0X^0");//set the quotient to zero
        Polynomial r = new ArithOpPoly(this);//set the remainder equal to the numerator
        Polynomial t = new ArithOpPoly();

        while(!r.isZero() && r.degree() >= p.degree()){//remainder not zero and degree of remainder is bigger than degree of denominator
            //divide the lead monomials
            t.getMonomials().add(new Monomial( r.lead().getCoefficient().doubleValue() / p.lead().getCoefficient().doubleValue(), r.lead().getPower() - p.lead().getPower()));
            q = q.plus(t);//add result to quotient
            r = r.minus(t.multiplyBy(p));//remove from the remainder the result multiplied by the denominator

            t.getMonomials().remove(0);
        }

        Polynomial[] result = new Polynomial[2];
        result[0] = q;
        result[1] = r;

        return result;
    }
}
