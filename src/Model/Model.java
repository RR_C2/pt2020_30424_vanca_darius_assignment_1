package Model;

import java.io.IOException;

public class Model {

    private ArithOpPoly firstPoly;
    private ArithOpPoly secondPoly;

    public void setFirstPoly(String poly) throws IOException {

        try {

            Validation.validate(poly);
            firstPoly = new ArithOpPoly(poly);

        }catch (IOException e) {
            throw e;
        }
    }

    public void setSecondPoly(String poly) throws IOException {
        try {

            Validation.validate(poly);
            secondPoly = new ArithOpPoly(poly);

        }catch (IOException e) {
            throw e;
        }
    }

    public Polynomial getFirstPoly(){
        return firstPoly;
    }

    public Polynomial getSecondPoly() {
        return secondPoly;
    }

    public Polynomial calculateSum(){
        return firstPoly.plus(secondPoly);
    }

    public Polynomial calculateDifference(){
        return firstPoly.minus(secondPoly);
    }

    public Polynomial multiply(){
        return firstPoly.multiplyBy(secondPoly);
    }

    public Polynomial integrate(){
        return firstPoly.integrate();
    }

    public Polynomial derivate(){ return firstPoly.derivate(); }

    public Polynomial[] divideBy()throws Exception{ return firstPoly.divideBy(secondPoly); }
}
