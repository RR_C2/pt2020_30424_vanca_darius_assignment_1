package Model;

public class Monomial implements Comparable<Monomial>{

    private Number coefficient;
    private int power;
/**
 * @param coefficient the coefficient of the monomial of type Number to allow flexibility
 * @param power the power of the monomial
 * **/
    public Monomial(Number coefficient, int power){

        this.coefficient = coefficient;
        this.power = power;
    }

    public Number getCoefficient() {
        return coefficient;
    }

    public int getPower() {
        return power;
    }

    public void setCoefficient(Number coefficient) {
        this.coefficient = coefficient;
    }

    public void setPower(int power) {
        this.power = power;
    }

    /**
     * @param o another monomial, comparison occurs based on the power
     * @return number < 0 if power is smaller, 0 if equal, > 0 if power is bigger
     * **/
    @Override
    public int compareTo(Monomial o) {
        return Integer.valueOf(power).compareTo(o.power);
    }
}
