package Tests;
import Model.ArithOpPoly;
import Model.Polynomial;
import com.sun.org.apache.bcel.internal.generic.ANEWARRAY;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@RunWith(Parameterized.class)
public class TestDivision {

    String poly1;
    String poly2;
    String expectedQ;
    String expectedR;

    public TestDivision(String poly1, String poly2, String expectedQ, String expectedR){

        this.poly1 = poly1;
        this.poly2 = poly2;
        this.expectedQ = expectedQ;
        this.expectedR = expectedR;
    }

    @Parameterized.Parameters(name = "{index} : testDivision(({0})/({1})) = Q:({2}) r:({3})")
    public static Collection<Object[]> data(){

        return Arrays.asList(new Object[][]{
                {"X^2+2X+1", "X+1", "X+1", "0"},
                {"X^3+1", "X^2-X+1", "X+1", "0"},
                {"X^3-3X^2+3X-1", "X^2-2X+1", "X-1", "0"},
                {"X^3+1", "X^2+2X", "X-2", "4X+1"},
                {"X^3+2X-1", "-3X+1", "-0.33X^2-0.11X-0.7", "-0.3"}
        });
    }

    @Test
    public void testBasicOperations(){

        ArithOpPoly polynomial1 = new ArithOpPoly(poly1);
        ArithOpPoly polynomial2 = new ArithOpPoly(poly2);

        Polynomial[] result = new Polynomial[2];
        try {
            result = polynomial1.divideBy(polynomial2);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }

        assertThat(result[0].toString(), is(expectedQ));
        assertThat(result[1].toString(), is(expectedR));

    }
}
