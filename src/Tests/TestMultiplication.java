package Tests;
import Model.ArithOpPoly;
import Model.Polynomial;
import com.sun.org.apache.bcel.internal.generic.ANEWARRAY;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@RunWith(Parameterized.class)
public class TestMultiplication {

    String poly1;
    String poly2;
    String expected;

    public TestMultiplication(String poly1, String poly2, String expected){

        this.poly1 = poly1;
        this.poly2 = poly2;
        this.expected = expected;
    }

    @Parameterized.Parameters(name = "{index} : testMultiplication(({0})*({1})) = ({2})")
    public static Collection<Object[]> data(){

        return Arrays.asList(new Object[][]{
                {"X^2+2", "0", "0"},
                {"X^3+2X^2+2", "7X^2", "7X^5+14X^4+14X^2"},
                {"5X^3+7", "X^3+3", "5X^6+22X^3+21"},
                {"-X^5+3X^4", "X^3-3", "-X^8+3X^7+3X^5-9X^4"},
                {"-99X^100", "X^2-2X+100", "-99X^102+198X^101-9900X^100"}
        });
    }

    @Test
    public void testBasicOperations(){

        ArithOpPoly polynomial1 = new ArithOpPoly(poly1);
        ArithOpPoly polynomial2 = new ArithOpPoly(poly2);
        assertThat(polynomial1.multiplyBy(polynomial2).toString(), is(expected));

    }
}
