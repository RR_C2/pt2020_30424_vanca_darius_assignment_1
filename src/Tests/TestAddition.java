package Tests;
import Model.ArithOpPoly;
import Model.Polynomial;
import com.sun.org.apache.bcel.internal.generic.ANEWARRAY;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@RunWith(Parameterized.class)
public class TestAddition {

    String poly1;
    String poly2;
    String expected;

    public TestAddition(String poly1, String poly2, String expected){

        this.poly1 = poly1;
        this.poly2 = poly2;
        this.expected = expected;
    }

    @Parameterized.Parameters(name = "{index} : testAddition(({0})+({1})) = ({2})")
    public static Collection<Object[]> data(){

        return Arrays.asList(new Object[][]{
                {"X^2+1", "X^3+2X^2-1", "X^3+3X^2"},
                {"X^100", "X^99+10X^30-50", "X^100+X^99+10X^30-50"},
                {"X^3-2X-3", "-3X^3-2X+4", "-2X^3-4X+1"},
                {"7X^2+2X^2-X^2", "10X-3", "8X^2+10X-3"},
                {"13X^5+4X^2-3X+20", "17X^5+22-4X", "30X^5+4X^2-7X+42"}
        });
    }

    @Test
    public void testBasicOperations(){

        ArithOpPoly polynomial1 = new ArithOpPoly(poly1);
        ArithOpPoly polynomial2 = new ArithOpPoly(poly2);
        assertThat(polynomial1.plus(polynomial2).toString(), is(expected));
    }
}

