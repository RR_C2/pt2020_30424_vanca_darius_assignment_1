package Tests;

import Model.ArithOpPoly;
import Model.Polynomial;
import com.sun.org.apache.bcel.internal.generic.ANEWARRAY;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@RunWith(Parameterized.class)
public class TestIntegration {

    String poly;
    String expected;

    public TestIntegration(String poly, String expected){

        this.poly = poly;
        this.expected = expected;
    }

    @Parameterized.Parameters(name = "{index} : testIntegration({0}) = ({1})")
    public static Collection<Object[]> data(){

        return Arrays.asList(new Object[][]{
                {"3X^2+2", "X^3+2X"},
                {"15X^4+9X^2-1", "3X^5+3X^3-X"},
                {"X^3+2X+1", "0.25X^4+X^2+X"},
                {"7X^3-5X", "1.75X^4-2.5X^2"},
                {"99X^32+17X^13-4X", "3X^33+1.21X^14-2X^2"}
        });
    }

    @Test
    public void testBasicOperations(){

        ArithOpPoly polynomial = new ArithOpPoly(poly);
        assertThat(polynomial.integrate().toString(), is(expected));

    }
}
