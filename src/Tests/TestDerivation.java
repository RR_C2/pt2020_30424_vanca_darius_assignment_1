package Tests;

import Model.ArithOpPoly;
import Model.Polynomial;
import com.sun.org.apache.bcel.internal.generic.ANEWARRAY;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@RunWith(Parameterized.class)
public class TestDerivation {

    String poly;
    String expected;

    public TestDerivation(String poly, String expected){

        this.poly = poly;
        this.expected = expected;
    }

    @Parameterized.Parameters(name = "{index} : testDerivation({0})' = ({1})")
    public static Collection<Object[]> data(){

        return Arrays.asList(new Object[][]{
                {"100", "0"},
                {"100X^99", "9900X^98"},
                {"X^3+2X+1", "3X^2+2"},
                {"12X^3-5X^2", "36X^2-10X"},
                {"-9X^5+17X^2-4X", "-45X^4+34X-4"}
        });
    }

    @Test
    public void testBasicOperations(){

        ArithOpPoly polynomial = new ArithOpPoly(poly);
        assertThat(polynomial.derivate().toString(), is(expected));

    }
}
