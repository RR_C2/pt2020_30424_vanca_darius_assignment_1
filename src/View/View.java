package View;


import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.IOException;
import javax.swing.JPanel;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;

public class View extends JFrame{

    private JTextField textField1 = new JTextField();
    private JTextField textField2 = new JTextField();
    private JLabel first = new JLabel();
    private JLabel second = new JLabel();
    private JLabel result = new JLabel();

    private String[] choices = {"Addition", "Subtraction", "Multiplication", "Division", "Integration", "Derivation"};

    private JPanel mainPanel = new JPanel();
    private JPanel secPanel1 = new JPanel();
    private JPanel secPanel2 = new JPanel();
    private JPanel secPanel3 = new JPanel();


    private JComboBox options = new JComboBox(choices);
    private JButton doIt = new JButton("GO");

    public View(){

        this.setPreferredSize(new Dimension(600,350));

        mainPanel.setBackground(Color.DARK_GRAY);
        mainPanel.setForeground(Color.DARK_GRAY);
        mainPanel.setPreferredSize(new Dimension(600,300));

        JLabel background = new JLabel();
        background.setIcon(new ImageIcon("E:\\Garbage\\Pic\\Nq1HPS.png"));
        background.setLayout(new BorderLayout());

        doIt.setBackground(Color.darkGray);
        doIt.setForeground(Color.lightGray);
        doIt.setSize(new Dimension(50,20));
        doIt.setFont(new Font("Montserrat", Font.BOLD, 16));

        secPanel1.add(options);
        secPanel1.add(doIt);
        secPanel1.setPreferredSize(new Dimension(400, 60));
        secPanel1.setBackground(new Color(50,50,50));
        secPanel1.setBorder(new EmptyBorder(10,10,10,10));

        textField1.setPreferredSize(new Dimension(255, 30));
        textField2.setPreferredSize(new Dimension(255, 30));

        textField1.setBorder(BorderFactory.createCompoundBorder(textField1.getBorder(), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        textField2.setBorder(BorderFactory.createCompoundBorder(textField2.getBorder(), BorderFactory.createEmptyBorder(5, 5, 5, 5)));

        options.setBackground(Color.darkGray);
        options.setForeground(Color.lightGray);
        options.setPreferredSize(new Dimension(200, 26));
        options.setFont(new Font("Montserrat", Font.BOLD, 16));

        textField1.setBackground(Color.darkGray);
        textField1.setForeground(Color.lightGray);

        textField2.setBackground(Color.darkGray);
        textField2.setForeground(Color.lightGray);

        textField1.setFont(new Font("Consolas", Font.BOLD, 16));
        textField1.setCaretColor(Color.lightGray);

        textField2.setFont(new Font("Consolas", Font.BOLD, 16));
        textField2.setCaretColor(Color.lightGray);

        secPanel2.setBackground(new Color(50,50,50));
        secPanel2.setBorder(new EmptyBorder(10,10,10,10));

        secPanel2.setPreferredSize(new Dimension(400, 100));
        secPanel2.add(textField1);
        secPanel2.add(textField2);

        result.setPreferredSize(new Dimension(380, 35));
        result.setBackground(new Color(50,50,50));
        result.setForeground(Color.lightGray);
        result.setFont(new Font("Consolas", Font.ITALIC, 16));

        secPanel3.setPreferredSize(new Dimension(400, 60));
        secPanel3.setBackground(new Color(50,50,50));
        secPanel3.add(result);


        mainPanel.add(secPanel1);
        mainPanel.add(secPanel2);
        mainPanel.add(secPanel3);
        mainPanel.setBorder(new EmptyBorder(25, 10, 10, 10));



        this.setContentPane(mainPanel);
        this.setVisible(true);
        this.pack();

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    public String getFirstPoly(){

        return textField1.getText();
    }

    public String getSecondPoly(){

        return textField2.getText();
    }

    public void setTextField1(String newString){

        textField1.setText(newString);
    }

    public void setTextField2(String newString){

        textField2.setText(newString);
    }

    public void setResult(String newString){

        result.setText(newString);
    }

    public String getOption() {

        return (String) options.getSelectedItem();
    }

    public void addGoButtonListener(ActionListener gbl){

        doIt.addActionListener(gbl);
    }

    public void addCBListener(ActionListener cbl){

        options.addActionListener(cbl);
    }

    public void setTextField1Visible(boolean val){
        this.textField1.setVisible(val);
    }

    public void setTextField2Visible(boolean val){
        this.textField2.setVisible(val);
    }

}
